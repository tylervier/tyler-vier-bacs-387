﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2ISBN
{
    class ISBNCheck
    {

        public string isbn { get; set; }

        public string CheckDigit10 (string isbn)
        {
            int sum = 0;
            for (int i = 0; i < 9; i++)
                sum += (10 - i) * Int32.Parse(isbn[i].ToString());
            int rem = sum % 11;
            int digit = 11 - rem;
            if (digit == 10)
                return ("X");
            else
                return (digit.ToString());
        }
        
        public string CheckDigit13 (string isbn)
        {
            int sum = 0;
            for (int i = 0; i < 12; i++)
                sum += (i % 2 ==0) ? 1 : 3 * Int32.Parse(isbn[i].ToString());
            int rem = sum % 10;
            int digit = 10 - rem;
            if (digit == 10)
                return ("0");
            else
                return (digit.ToString());
        }

    }
}
