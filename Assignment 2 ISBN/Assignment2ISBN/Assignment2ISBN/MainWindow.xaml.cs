﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Assignment2ISBN
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            ISBNCheck number = new ISBNCheck();

            string isbn = ISBN_txtbox.Text;
            int len = isbn.Length;

            if (len ==9)
            {
                string digit = number.CheckDigit10(isbn);
                lblCheckSymbol.Content = ("Check symbol is " + digit);
                lblFullNumber.Content = ("The 10 digit ISBN is " + isbn + "-" + digit);
            }
            else if (len == 12)
            {
                string digit = number.CheckDigit10(isbn);
                lblCheckSymbol.Content = ("Check symbol is " + digit);
                lblFullNumber.Content = ("The 13 digit ISBN is " + isbn + "-" + digit);
            }
            else
            {
                lblCheckSymbol.Content = ("Invalid ISBN Length: Please try again");
                lblFullNumber.Content = ("");
            }
        }
    }
}
