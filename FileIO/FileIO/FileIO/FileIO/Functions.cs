﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIO
{
    class Functions
    {
        public void writeFile(Data data)
        {
            StreamWriter sw = new StreamWriter(data.file, true);
            if(data.gender == true)
            {
                sw.WriteLine("Name: " + data.name + "   Age: " + data.age + "   Gender: Male " + "   Birthday: " + data.date);
            }
            else
            {
                sw.WriteLine("Name: " + data.name + "   Age: " + data.age + "   Gender: Female " + "   Birthday: " + data.date);
            }
            sw.Close();

        }
        public string[] readFile(Data data)
        {
            string[] readFile = new string[50];
            int i = 0;
            foreach (string line in File.ReadAllLines(data.file))
            {
                readFile[i] = line;
                i++;
            }
            return readFile;
        }
    }
}
