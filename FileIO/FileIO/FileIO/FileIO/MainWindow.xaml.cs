﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace FileIO
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog of = new OpenFileDialog();
            of.ShowDialog();
            txtFile.Text = of.FileName;
        }

        private void btnWrite_Click(object sender, RoutedEventArgs e)
        {
            Data writedata = new Data();
            writedata.file = txtFile.Text;
            writedata.name = txtName.Text;
            writedata.age = Convert.ToInt32(txtAge.Text);
            writedata.gender = txtGender.Text == "m" ? true : false;
            writedata.date = DateTime.Parse(txtBirthday.Text);

            Functions function = new Functions();
            function.writeFile(writedata);
        }

        private void btnRead_Click(object sender, RoutedEventArgs e)
        {
            Functions function = new Functions();
            Data readdata = new Data();
            readdata.file = txtFile.Text;
            string[] printArray = function.readFile(readdata);
            lstRead.Items.Clear();
            string printString = "";
            for (int i = 0; i < 50; i++)
            {
                printString = (printArray[i]);
                lstRead.Items.Add(printString);
                printString = "";
            }

        }
    }
}